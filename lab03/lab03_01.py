import turtle
def draw_ten(t, lenth):
    n = int(input())
    for i in range(n):
        a=180-(((n-2)*180)/n)
        t.forward(lenth);
        t.left(a)

def main():
    win = turtle.Screen()
    win.bgcolor("lightgreen"); win.title("Ten")
    bob = turtle.Turtle(); bob.color("Blue")
    bob.pensize(3); draw_ten(bob, 150)
    win.mainloop()

main()
